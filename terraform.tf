terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.24.0"
    }
    vault = {
      source = "hashicorp/vault"
      version = "3.23.0"
    }
  }

  # backend "remote" {
  #   hostname = "app.terraform.io"
  #   organization = "example-org-15045d"
  #   token = "1zbnMnGo2kez9A.atlasv1.pzpbzzTfzSYIyowbUdPHpmhOXNdg2LPFB2FEzLpTsHTD6sHv4cUUiWm7fz2euURKnJg"

  #   workspaces {
  #     name = "mylab01"
  #   }
  # }

  backend "s3" {
    bucket = "tf-rmt-be"
    key    = "cep/mylab/terraform-state"
    region = "us-east-1"
    dynamodb_table = "tf-rmt-be"
  }

}