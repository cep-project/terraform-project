# General
## HashiCorp Vault

hcv_address = "https://vault-cluster-public-vault-5994a63e.f63de60e.z1.hashicorp.cloud:8200"
hcv_token = "hvs.CAESIPyROJGHQgU7ka_Z-2_FbtRUbBnFuhrAU4MtAwIdZwTIGigKImh2cy5Telh1UTZPNnhWZ0JNa1NOTnJ3Z0VWdDMualNmbmgQus8E"

## AWS
aws_access_key = "AKIAQECLTJFPM5O2XMPW"
aws_secret_key = "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"
region     = "us-east-1"
tags = {
  createdBy   = "Terraform"
  author      = "khangtictoc"
  environment = "dev"
  project     = "lab01"
}

# HashiCorp Vault Management
vault_secret_path = {
    ubuntu20 = "kv/mylab01/ubuntu20"
    winserver2019 = "kv/mylab01/winserver2019"
}

# Compute

vm_user = {
    ubuntu = "ubuntu"
    winserver = "Administrator"
}

vm_name = {
    ubuntu20 = "ubuntu20"
    winserver2019 = "winserver2019"
}

vm_ami = {
    ubuntu20 = "ami-06aa3f7caf3a30282"
    winserver2019 = "ami-0cd601a22ac9e6d79"
}

instance_type = {
    ubuntu20 = "t2.micro"
    winserver2019 = "t2.medium"
}
keypair_name = "my-keypair"

# Network

security_group_linux = {
    name        = "linux-secgroup"
    description = "Allow SSH-inbound, HTTP/HTTPS-outbound traffic"
}

security_group_rule_linux = {
    ingress = {
        allow_ssh = {
            from_port   = 22
            to_port     = 22
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
        allow_http = {
            from_port   = 80
            to_port     = 80
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }
    egress = {
        allow_http = {
            from_port   = 80
            to_port     = 80
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
        allow_https = {
            from_port   = 443
            to_port = 443
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }
}

security_group_win = {
    name        = "winserver-secgroup"
    description = "Allow RDP/WinRM/PingReply/PingRequest-inbound, HTTP/HTTPS-outbound traffic"
}

security_group_rule_win = {
    ingress = {
        allow_winrm_https = {
            from_port   = 5986
            to_port     = 5986
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
        allow_rdp = {
            from_port   = 3389
            to_port     = 3389
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
        allow_ping_reply = {
            from_port   = 0
            to_port     = 0
            protocol    = "icmp"
            cidr_blocks = ["0.0.0.0/0"]
        }
        allow_ping_request = {
            from_port   = 8
            to_port     = 0
            protocol    = "icmp"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }
    egress = {
        allow_http = {
            from_port   = 80
            to_port     = 80
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
        allow_https = {
            from_port   = 443
            to_port = 443
            protocol    = "tcp"
            cidr_blocks = ["0.0.0.0/0"]
        }
    }
}

# S3

s3_rmt_be_bucket = {
    name = "rmt-be-bucket"
    ownership = "BucketOwnerPreferred"
    acl = "private"
}
