# Environment configurations

## HashiCorp Vault

variable "hcv_address" {
  type        = string
  description = "HashiCorp Vault Address"
}

variable "hcv_token" {
  type        = string
  description = "HashiCorp Vault token"
}

## AWS

variable "aws_access_key" {
  type        = string
  description = "AWS Access Key"
}

variable "aws_secret_key" {
  type        = string
  description = "AWS Secret Key"
}

variable "region" {
  type        = string
  description = "Using region"
}

variable "tags" {
  type = object({
    createdBy   = string
    author      = string
    environment = string
    project     = string
  })
  description = "Metadata to assign to all resources"
}

# HashiCorp Vault Management

variable "vault_secret_path" {
  description = "Vault secret path configuration"
  type = map(string)
}

# Compute

variable "vm_user" {
  description = "VM user configuration"
  type = map(string)
}

variable "vm_name" {
  description = "VM name configuration"
  type = map(string)
}

variable "vm_ami" {
  description = "VM AMI configuration"
  type = map(string)
}

variable "instance_type" {
  description = "Instance type configuration"
  type = map(string)
}

variable "keypair_name" {
  type        = string
  description = "Keypair name"
}

# Network

variable "security_group_linux" {
  description = "Security group details allowing SSH"
  type = object({
    name        = string
    description = string
  })
}

variable "security_group_rule_linux" {
  description = "Security group rules for Linux"
  type = object({
    ingress = object({
      allow_ssh = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
      allow_http = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
    })
    egress = object({
      allow_http = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
      allow_https = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
    })
  })
}

variable "security_group_win" {
  description = "Security group details allowing RDP"
  type = object({
    name        = string
    description = string
  })
}

variable "security_group_rule_win" {
  description = "Security group rules for Windows"
  type = object({
    ingress = object({
      allow_winrm_https = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
      allow_rdp = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
      allow_ping_reply = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
      allow_ping_request = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
    })
    egress = object({
      allow_http = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
      allow_https = object({
        from_port   = number
        to_port     = number
        protocol    = string
        cidr_blocks = list(string)
      })
    })
  })
}

# S3

variable "s3_rmt_be_bucket" {
  description = "S3 bucket for remote backend"
  type = object({
    name = string
    ownership = string
    acl = string
  })
}

# DynamoDB

