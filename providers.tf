provider "aws" {
  region     = var.region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

provider "vault" {
  address = var.hcv_address
  token  = var.hcv_token
}