module "keypair" {
    source = "./keypair"

    keypair_name = var.keypair_name
}

module "linux_instance" {
    source = "./instance"

    instance_name = var.vm_name.ubuntu20
    security_group_ids = var.security_group_linux_ids
    ami_id        = var.vm_ami.ubuntu20
    get_password_data = false
    instance_type = var.instance_type.ubuntu20
    keypair_name  = module.keypair.keypair_name
    tags          = var.tags
}

module "win_instance" {
    source = "./instance"

    instance_name = var.vm_name.winserver2019
    security_group_ids = var.security_group_win_ids
    ami_id        = var.vm_ami.winserver2019
    get_password_data = true
    instance_type = var.instance_type.winserver2019
    keypair_name  = module.keypair.keypair_name
    tags          = var.tags
}

resource "local_file" "ssh_key" {
    content  = module.keypair.ssh_private_key
    filename = "credentials/ssh-key/myKey.pem"
}

locals {
    winserver_password = rsadecrypt(
        module.win_instance.instance_password_data,
        module.keypair.ssh_private_key
    )
}

resource "local_file" "linux_private_key" {
    content = <<-EOT
        host: ${module.win_instance.dns_public}
        password: ${local.winserver_password}
    EOT
    filename = "credentials/rdp-connect/connectionString.txt"
}

resource "vault_generic_secret" "winserver" {
    path = var.vault_secret_path.winserver2019

    data_json = <<-EOT
    {
        "host":   "${module.win_instance.dns_public}",
        "username": "${var.vm_user.winserver}",
        "password": "${local.winserver_password}"
    }
    EOT
}

resource "vault_generic_secret" "ubuntu" {
    path = var.vault_secret_path.ubuntu20

    data_json = jsonencode(
        {
            "host":   "${module.linux_instance.dns_public}",
            "username": "${var.vm_user.ubuntu}",
            "private_ssh_key": "${module.keypair.ssh_private_key}"
        }
    )
}