variable keypair_name {}
variable vm_name {}
variable instance_type {}
variable vm_ami {}
variable vm_user {}
variable security_group_linux_ids {}
variable security_group_win_ids {}
variable "vault_secret_path" {}

variable tags {}