output instance_password_data {
    value = aws_instance.instance.password_data
    description = "The password data for the instance"
}

output dns_public {
    value = aws_instance.instance.public_dns
    description = "The public DNS name assigned to the instance"
}