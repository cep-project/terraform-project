<powershell>

# Disable IE Enhanced Security Configuration (ESC)

$AdminKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
$UserKey = "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}"
Set-ItemProperty -Path $AdminKey -Name "IsInstalled" -Value 0
Set-ItemProperty -Path $UserKey -Name "IsInstalled" -Value 0
Stop-Process -Name Explorer
Write-Host "IE Enhanced Security Configuration (ESC) has been disabled." -ForegroundColor Green

# Install WinRM and establish a connection with listeners

$url = "https://raw.githubusercontent.com/AlbanAndrieu/ansible-windows/master/files/ConfigureRemotingForAnsible.ps1"
$filePath = "C:\Users\Administrator\Desktop\ConfigureRemotingForAnsible.ps1"

Invoke-WebRequest -Uri $url -OutFile $filePath
Invoke-Expression -Command (Get-Content -Path $filePath -Raw)

# Enable ICMPv4-In Echo Request Rule in Windows Firewall Defender for `Ansible Ping Testing` purpose only

Set-NetFirewallRule -DisplayName "File and Printer Sharing (Echo Request - ICMPv4-In)" -Enabled True

</powershell>
