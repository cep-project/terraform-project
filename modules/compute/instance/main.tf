resource "aws_instance" "instance" {
  ami           = var.ami_id # This is an example Amazon Linux 2 AMI ID. Replace with the AMI ID for your desired Linux distribution.
  instance_type = var.instance_type

  get_password_data = var.get_password_data
  vpc_security_group_ids = [var.security_group_ids]
  key_name               = var.keypair_name
  user_data = file("${path.module}/script/winrm_conn.ps1")
  tags = merge(
    var.tags,
    {
      "Name" = var.instance_name
    }
  )
}

