resource "aws_security_group" "allow_linux" {
    name        = var.security_group_linux.name
    description = var.security_group_linux.description

    tags = merge(
        var.tags,
        {
          "Name" = "allow-ssh"
        }
    )
}

resource "aws_security_group_rule" "allow_ssh_rule_linux" {
  type              = "ingress"
  from_port         = var.security_group_rule_linux.ingress.allow_ssh.from_port
  to_port           = var.security_group_rule_linux.ingress.allow_ssh.to_port
  protocol          = var.security_group_rule_linux.ingress.allow_ssh.protocol
  cidr_blocks       = var.security_group_rule_linux.ingress.allow_ssh.cidr_blocks
  security_group_id = aws_security_group.allow_linux.id
}

resource "aws_security_group_rule" "allow_http_rule_linux_ingress" {
  type              = "ingress"
  from_port         = var.security_group_rule_linux.ingress.allow_http.from_port
  to_port           = var.security_group_rule_linux.ingress.allow_http.to_port
  protocol          = var.security_group_rule_linux.ingress.allow_http.protocol
  cidr_blocks       = var.security_group_rule_linux.ingress.allow_http.cidr_blocks
  security_group_id = aws_security_group.allow_linux.id
}

resource "aws_security_group_rule" "allow_https_rule_linux" {
  type              = "egress"
  from_port         = var.security_group_rule_linux.egress.allow_https.from_port
  to_port           = var.security_group_rule_linux.egress.allow_https.to_port
  protocol          = var.security_group_rule_linux.egress.allow_https.protocol
  cidr_blocks       = var.security_group_rule_linux.egress.allow_https.cidr_blocks
  security_group_id = aws_security_group.allow_linux.id
}

resource "aws_security_group_rule" "allow_http_rule_linux" {
  type              = "egress"
  from_port         = var.security_group_rule_linux.egress.allow_http.from_port
  to_port           = var.security_group_rule_linux.egress.allow_http.to_port
  protocol          = var.security_group_rule_linux.egress.allow_http.protocol
  cidr_blocks       = var.security_group_rule_linux.egress.allow_http.cidr_blocks
  security_group_id = aws_security_group.allow_linux.id
}

resource "aws_security_group" "allow_win" {
    name        = var.security_group_win.name
    description = var.security_group_win.description

    tags = merge(
    var.tags,
    {
      "Name" = "allow-rdp"
    }
  )
}

resource "aws_security_group_rule" "allow_winrm_https_rule" {
  type              = "ingress"
  from_port         = var.security_group_rule_win.ingress.allow_winrm_https.from_port
  to_port           = var.security_group_rule_win.ingress.allow_winrm_https.to_port
  protocol          = var.security_group_rule_win.ingress.allow_winrm_https.protocol
  cidr_blocks       = var.security_group_rule_win.ingress.allow_winrm_https.cidr_blocks
  security_group_id = aws_security_group.allow_win.id
}

resource "aws_security_group_rule" "allow_rdp_rule" {
  type              = "ingress"
  from_port         = var.security_group_rule_win.ingress.allow_rdp.from_port
  to_port           = var.security_group_rule_win.ingress.allow_rdp.to_port
  protocol          = var.security_group_rule_win.ingress.allow_rdp.protocol
  cidr_blocks       = var.security_group_rule_win.ingress.allow_rdp.cidr_blocks
  security_group_id = aws_security_group.allow_win.id
}

resource "aws_security_group_rule" "allow_ping_reply_rule" {
  type              = "ingress"
  from_port         = var.security_group_rule_win.ingress.allow_ping_reply.from_port
  to_port           = var.security_group_rule_win.ingress.allow_ping_reply.to_port
  protocol          = var.security_group_rule_win.ingress.allow_ping_reply.protocol
  cidr_blocks       = var.security_group_rule_win.ingress.allow_ping_reply.cidr_blocks
  security_group_id = aws_security_group.allow_win.id
}

resource "aws_security_group_rule" "allow_ping_request_rule" {
  type              = "ingress"
  from_port         = var.security_group_rule_win.ingress.allow_ping_request.from_port
  to_port           = var.security_group_rule_win.ingress.allow_ping_request.to_port
  protocol          = var.security_group_rule_win.ingress.allow_ping_request.protocol
  cidr_blocks       = var.security_group_rule_win.ingress.allow_ping_request.cidr_blocks
  security_group_id = aws_security_group.allow_win.id
}

resource "aws_security_group_rule" "allow_http_rule_win" {
  type              = "egress"
  from_port         = var.security_group_rule_win.egress.allow_http.from_port
  to_port           = var.security_group_rule_win.egress.allow_http.to_port
  protocol          = var.security_group_rule_win.egress.allow_http.protocol
  cidr_blocks       = var.security_group_rule_win.egress.allow_http.cidr_blocks
  security_group_id = aws_security_group.allow_win.id
}

resource "aws_security_group_rule" "allow_https_rule_win" {
  type              = "egress"
  from_port         = var.security_group_rule_win.egress.allow_https.from_port
  to_port           = var.security_group_rule_win.egress.allow_https.to_port
  protocol          = var.security_group_rule_win.egress.allow_https.protocol
  cidr_blocks       = var.security_group_rule_win.egress.allow_https.cidr_blocks
  security_group_id = aws_security_group.allow_win.id
}




