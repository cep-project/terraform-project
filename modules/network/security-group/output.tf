output security_group_linux {
    value = aws_security_group.allow_linux.id
    description = "The ID of the security group allow SSH"
}

output security_group_win {
    value = aws_security_group.allow_win.id
    description = "The ID of the security group allow RDP"
}