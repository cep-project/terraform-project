resource "aws_s3_bucket" "rmt_be_bucket" {
  bucket = var.bucket_name
}

resource "aws_s3_bucket_ownership_controls" "myownership" {
  bucket = aws_s3_bucket.rmt_be_bucket.id
  rule {
    object_ownership = var.ownership
  }
}

resource "aws_s3_bucket_acl" "example" {
  depends_on = [aws_s3_bucket_ownership_controls.myownership]

  bucket = aws_s3_bucket.rmt_be_bucket.id
  acl    = var.acl
}