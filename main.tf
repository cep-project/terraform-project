module "network" {
    source = "./modules/network/security-group"

    security_group_linux      = var.security_group_linux
    security_group_win    = var.security_group_win
    security_group_rule_linux = var.security_group_rule_linux
    security_group_rule_win   = var.security_group_rule_win
    tags                      = var.tags
}

module "compute" {
    source = "./modules/compute"

    vm_name = var.vm_name
    security_group_linux_ids = module.network.security_group_linux
    security_group_win_ids = module.network.security_group_win

    vm_user = var.vm_user
    vm_ami = var.vm_ami

    instance_type = var.instance_type
    keypair_name = var.keypair_name
    tags          = var.tags
    vault_secret_path = var.vault_secret_path
}

# module "s3" {
#     source = "./modules/s3"

#     bucket_name = var.s3_rmt_be_bucket.bucket_name
#     acl         = var.s3_rmt_be_bucket.acl
#     ownership   = var.s3_rmt_be_bucket.ownership
    
#     tags        = var.tags
# }